defmodule Todo.Auth.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias Todo.Repo

  schema "users" do
    field(:email, :string)
    field(:encrypted_password, :string)
    field(:username, :string)
    field(:password_confirmation, :string, virtual: true)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :username, :encrypted_password])
    |> validate_required([:email, :username, :encrypted_password])
    |> unique_constraint(:email)
    |> unique_constraint(:username)
  end

  def registration_changeset(%__MODULE__{} = user, attrs) do
    user
    |> changeset(attrs)
    |> validate_confirmation(:encrypted_password)
    |> cast(attrs, [:encrypted_password], [])
    |> validate_length(:encrypted_password, min: 6, max: 128)
    |> encrypt_password()
  end

  defp encrypt_password(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}} ->
        put_change(changeset, :encrypted_password, Comeonin.Bcrypt.hashpwsalt(password))

      _ ->
        changeset
    end
  end

  def sign_in(email, password) do
    user = Repo.get_by(__MODULE__, email: email)

    IO.inspect(email)

    cond do
      user && Comeonin.Bcrypt.checkpw(password, user.encrypted_password) ->
        {:ok, user}

      true ->
        {:error, :unauthorized}
    end
  end

  def current_user(conn) do
    user_id = Plug.Conn.get_session(conn, :current_user_id)
    if user_id, do: Repo.get(__MODULE__, user_id)
  end

  def user_signed_in?(conn) do
    !!current_user(conn)
  end

  def sign_out(conn) do
    Plug.Conn.configure_session(conn, drop: true)
  end

  def register(params) do
    %__MODULE__{}
    |> registration_changeset(params)
    |> Repo.insert()
  end
end
