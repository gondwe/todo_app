defmodule Todo.Todos.Todo do
  use Ecto.Schema
  import Ecto.Changeset


  schema "todos" do
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(todo, attrs) do
    todo
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name)
  end
end
