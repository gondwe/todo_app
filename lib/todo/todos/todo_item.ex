defmodule Todo.Todos.TodoItem do
  use Ecto.Schema
  import Ecto.Changeset
  alias Todo.Todos.Todo

  schema "todo_items" do
    field(:done, :boolean, default: false)
    field(:name, :string)
    belongs_to(:todo, Todo)

    timestamps()
  end

  @doc false
  def changeset(todo_item, attrs) do
    todo_item
    |> cast(attrs, [:name, :done])
    |> validate_required([:name, :done])
    |> unique_constraint(:name)
  end
end
