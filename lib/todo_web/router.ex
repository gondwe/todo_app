defmodule TodoWeb.Router do
  use TodoWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", TodoWeb do
    # Use the default browser stack
    pipe_through(:browser)

    get("/", PageController, :index)
    get("/session/new", SessionController, :new)
    post("/session/create", SessionController, :create)
    delete("/sign_out", SessionController, :delete)

    # registration
    resources("/registrations", RegistrationController, only: [:new, :create])
  end

  # Other scopes may use custom stacks.
  # scope "/api", TodoWeb do
  #   pipe_through :api
  # end
end
