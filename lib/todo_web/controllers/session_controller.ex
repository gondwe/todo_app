defmodule TodoWeb.SessionController do
  use TodoWeb, :controller
  alias Todo.Auth.User

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, %{"session" => %{"email" => email, "password" => password}}) do
    case User.sign_in(email, password) do
      {:ok, user} ->
        conn
        |> put_session(:current_user_id, user.id)
        |> put_flash(:info, "You have successfully signed in!")
        |> redirect(to: page_path(conn, :index))

      {:error, reason} ->
        conn
        |> put_flash(:error, "Invalid Email or Password")
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> User.sign_out()
    |> redirect(to: page_path(conn, :index))
  end
end
