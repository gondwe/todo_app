defmodule Todo.Repo.Migrations.CreateTodos do
  use Ecto.Migration

  def change do
    create table(:todos) do
      add :name, :string

      timestamps()
    end

    create unique_index(:todos, [:name])
  end
end
