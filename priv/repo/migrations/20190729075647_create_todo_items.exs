defmodule Todo.Repo.Migrations.CreateTodoItems do
  use Ecto.Migration

  def change do
    create table(:todo_items) do
      add(:name, :string)
      add(:todo_id, references("todos"))
      add(:done, :boolean, default: false, null: false)

      timestamps()
    end

    create(unique_index(:todo_items, [:name]))
  end
end
